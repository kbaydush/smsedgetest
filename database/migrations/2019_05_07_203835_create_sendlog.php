<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_log', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('usrId')->nullable();
            $table->integer('numId')->nullable();
            $table->string('logMessage')->nullable();
            $table->boolean('logSuccess');
        });
    }
//(log_id, usr_id, num_id, log_message, log_success (bool), log_created)
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendlog');
    }
}
