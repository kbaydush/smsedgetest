<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usr_id', 'num_id', 'log_message', 'log_success', 'log_created'
    ];
}
